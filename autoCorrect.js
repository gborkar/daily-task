/**
 * This function a)Add space after every punctuation mark(fullStop, comma, exclamation)
 * b)Start new word with capital letter
 * @param {string} input_string_paragraph 
 * @returns returns corrected paragraph
 */
const autoCorrector = (input_string_paragraph) =>
{
  let len = input_string_paragraph.length                                 // Length of input paragraph
  let correct_string = ""                                                 // Declaring an empty string
  correct_string += input_string_paragraph[0].toUpperCase();              // Making first letter to upperCase
  for(let i = 1; i<len; i++)
  {
    if(input_string_paragraph[i] == ","                                   // Checking comma or exclamation mark
    || input_string_paragraph[i] == "!" 
    ){
      correct_string +=  input_string_paragraph[i]+ " "                   // Adding space if found (, Or !)
    }
    else if(input_string_paragraph[i] == ".")                             // Checking for fullstop
    {
      correct_string +=  input_string_paragraph[i]+ " "                   // Adding space if . found
      let char_after_fullstop = input_string_paragraph[i+1]               // Getting letter after .                          
      if(char_after_fullstop!=undefined){
        char_after_fullstop = char_after_fullstop.toUpperCase()           // Changing that letter to uppercase
        correct_string +=  char_after_fullstop                            // Adding to result string
        i++;
      }
    }
    else{
    correct_string += input_string_paragraph[i]                          //Just adding elements to 
    }
  }
  correct_string.trim()                                                  //To remove whitespaces from string
  return correct_string
}
/**
 * This is the entry point of code which take input and returns output
 * @param {string} input 
 */
const mainFunction = (input)=>{
  console.log("Input/Incorrect paragraph :\n",input)
  let correctParagraph = autoCorrector(input)
  console.log("Correct paragraph: \n", correctParagraph)
}
let paragraph = "what a life!motivation is an essential factor that changes positive thought into instant action.it switches a great idea into action and can undoubtedly affect the world around you.however,not all are born with motivation.people sometimes have disbelief in themselves,being demotivated means living a life as a worn-out machine.your life will become dull without any spark.So,to gain inner peace and satisfaction in life,you must always stay motivated."

mainFunction(paragraph)